<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <title>Editer les sous-titres</title>
</head>
<body>
<h3 style="margin-left: 20px; margin-top: 10px" id="test">Le traducteur de sous-titres</h3>
<form method="post">
    <button style="position:fixed; top: 10px; right: 10px; margin-right: 20px" type="submit" class="btn btn-warning btn-md">Enregistrer</button>
    <a style="margin-bottom: 40px; margin-left: 20px; margin-top: 10px" class="btn btn-outline-info" href="UploadServlet" role="button">Ajouter un fichier de sous-titres</a>
    <table>
        <c:forEach items="${ subtitles }" var="subtitle" varStatus="status">
            <input type="hidden" name="num" value="${ subtitle.num }">
            <input type="hidden" name="beginEnd" value="${ subtitle.beginEndSub }">
            <c:choose>
                <c:when test="${subtitle.scdTextSub != null && !subtitle.scdTextSub.equals('')}">
                    <tr>
                        <td style="text-align:right;"><c:out value="${ subtitle.firstTextSub } "/></td>
                        <td><input style="margin-left: 10px" class="form-control" type="text" name="firstSub" id="firstSub${ status.index }" size="35" value="${subtitlesEng[status.index].firstTextSub}" /></td>
                    </tr>
                    <tr>
                        <td style="text-align:right;"><c:out value="${ subtitle.scdTextSub } "/></td>
                        <td><input style="margin-left: 10px" class="form-control" type="text" name="scdSub" id="scdSub${ status.index }" size="35" value="${subtitlesEng[status.index].scdTextSub}" /></td>
                    </tr>
                </c:when>
                <c:when test="${subtitle.scdTextSub == null || subtitle.scdTextSub.equals('')}">
                    <tr>
                        <td style="text-align:right;"><c:out value="${ subtitle.firstTextSub } " /></td>
                        <td><input style="margin-left: 10px" class="form-control" type="text" name="firstSub" id="firstSub${ status.index }" size="35" value="${subtitlesEng[status.index].firstTextSub}" /></td>
                        <td><input type="hidden" name="scdSub" id="scdSub${ status.index }" size="35" value="${subtitlesEng[status.index].scdTextSub}" /></td>
                    </tr>
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
        </c:forEach>
    </table>
</form>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>