package com.subtitlor.servlets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.subtitlor.beans.Subtitle;
import com.subtitlor.dao.DaoFactory;
import com.subtitlor.dao.SubtitleDao;
import com.subtitlor.utilities.WriteInFile;

@WebServlet("/EditSubtitle")
public class EditSubtitle extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String fileFr = "/WEB-INF/password_presentation.srt";
    private static final String fileEng = "/WEB-INF/password_presentation_eng.srt";
    private SubtitleDao subtitleDao;

    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.subtitleDao = daoFactory.getSubtitleDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = getServletContext();
        subtitleDao.create(context.getRealPath(fileFr));

        request.setAttribute("subtitles", subtitleDao.getAll("fr"));
        request.setAttribute("subtitlesEng", subtitleDao.getAll("eng"));

        this.getServletContext().getRequestDispatcher("/WEB-INF/edit_subtitle.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = getServletContext();

        String[] nums = request.getParameterValues("num");
        String[] beginEnds = request.getParameterValues("beginEnd");
        String[] firstSubs = request.getParameterValues("firstSub");
        String[] scdSubs = request.getParameterValues("scdSub");

        for (int i = 0; i < firstSubs.length; i++) {
            Subtitle subtitle = new Subtitle();
            subtitle.setNum(Integer.valueOf(nums[i]));
            subtitle.setBeginEndSub(beginEnds[i]);
            subtitle.setFirstTextSub(firstSubs[i]);
            subtitle.setScdTextSub(scdSubs[i]);
            subtitleDao.add(subtitle);
        }

        List<Subtitle> subsList = subtitleDao.getAll("eng");

        try {
            WriteInFile.write(subsList,context.getRealPath(fileEng));
        }catch (FileNotFoundException e){
            e.getMessage();
        }

        doGet(request,response);
    }
}
