package com.subtitlor.dao;

import com.subtitlor.beans.Subtitle;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SubtitleDaoImpl implements SubtitleDao {

    private DaoFactory daoFactory;
    private ArrayList<String> originalSubtitles = null;

    SubtitleDaoImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public void closeConnections(Connection connexion){
        try{
            if (connexion != null) {
                connexion.close();
            }
        }catch (SQLException e){
        }
    }

    @Override
    public List<Subtitle> getAll(String language){
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        List<Subtitle> subsList = new ArrayList<Subtitle>();

        try {
            connexion = daoFactory.getConnection();
            if (language.equals("fr")){
                preparedStatement = connexion.prepareStatement("SELECT * FROM pass_pres_fr;");
            }else{
                preparedStatement = connexion.prepareStatement("SELECT * FROM pass_pres_eng;");
            }
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                Subtitle sub = new Subtitle();
                sub.setNum(rs.getInt("num"));
                sub.setBeginEndSub(rs.getString("begin_end"));
                sub.setFirstTextSub(rs.getString("first_text_sub"));
                sub.setScdTextSub(rs.getString("scd_text_sub"));
                subsList.add(sub);
            }
            return subsList;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeConnections(connexion);
        }
        return null;
    }

    @Override
    public void create(String file){
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        int count = 0;

        //Check if database already create
        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM pass_pres_fr;");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                count += 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeConnections(connexion);
        }

        if (count == 0){
            String subToAdd[] = new String[4];
            int i = 0;
            int j = 0;

            originalSubtitles = new ArrayList<String>();
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    originalSubtitles.add(line);
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            while (i < originalSubtitles.size()){
                j = 0;
                for (int k = 0; k < 4; k++){
                    subToAdd[k] = "";
                }
                while (i < originalSubtitles.size() && j < 4){
                    if (j == 3 && originalSubtitles.get(i).length() < 2){
                        i -= 1;
                    }else{
                        subToAdd[j] = originalSubtitles.get(i);
                    }
                    j++;
                    i++;
                }
                if(!subToAdd[3].equals("")){
                    try {
                        connexion = daoFactory.getConnection();
                        preparedStatement = connexion.prepareStatement(
                                "INSERT INTO pass_pres_fr(num,begin_end,first_text_sub,scd_text_sub) " +
                                        "VALUES(?,?,?,?);");
                        preparedStatement.setInt(1, Integer.valueOf(subToAdd[0]));
                        preparedStatement.setString(2, subToAdd[1]);
                        preparedStatement.setString(3, subToAdd[2]);
                        preparedStatement.setString(4, subToAdd[3]);
                        preparedStatement.executeUpdate();

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }finally {
                        closeConnections(connexion);
                    }
                }else{
                    try {
                        connexion = daoFactory.getConnection();
                        preparedStatement = connexion.prepareStatement(
                                "INSERT INTO pass_pres_fr(num,begin_end,first_text_sub) " +
                                        "VALUES(?,?,?);");
                        preparedStatement.setInt(1, Integer.valueOf(subToAdd[0]));
                        preparedStatement.setString(2, subToAdd[1]);
                        preparedStatement.setString(3, subToAdd[2]);
                        preparedStatement.executeUpdate();

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }finally {
                        closeConnections(connexion);
                    }
                }
                i += 1;
            }
        }
    }

    @Override
    public void add(Subtitle subtitle) {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("SELECT * FROM pass_pres_eng WHERE num = ?;");
            preparedStatement.setInt(1, subtitle.getNum());
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.isBeforeFirst()) {
                preparedStatement = connexion.prepareStatement("INSERT INTO pass_pres_eng(begin_end,first_text_sub,scd_text_sub,num) " +
                        "VALUES(?,?,?,?);");
            }else{
                preparedStatement = connexion.prepareStatement("UPDATE pass_pres_eng " +
                        "SET begin_end = ?, first_text_sub = ?, scd_text_sub = ? " +
                        "WHERE num = ?;");
            }

            preparedStatement.setString(1,subtitle.getBeginEndSub());
            preparedStatement.setString(2, subtitle.getFirstTextSub());
            preparedStatement.setString(3,subtitle.getScdTextSub());
            preparedStatement.setInt(4, subtitle.getNum());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnections(connexion);
        }
    }

    @Override
    public List<Subtitle> list() {
        List<Subtitle> subtitles = new ArrayList<Subtitle>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT first_text_sub FROM pass_pres_eng;");

            while (resultat.next()) {
                String textSub = resultat.getString("text_sub");
                Subtitle subtitle = new Subtitle();
                subtitle.setFirstTextSub(textSub);
                subtitles.add(subtitle);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeConnections(connexion);
        }
        return subtitles;
    }

}
