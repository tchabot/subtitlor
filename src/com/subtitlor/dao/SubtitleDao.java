package com.subtitlor.dao;

import com.subtitlor.beans.Subtitle;

import java.sql.ResultSet;
import java.util.List;

public interface SubtitleDao {
    void add( Subtitle subtitle);
    List<Subtitle> list();
    void create(String file);
    List<Subtitle> getAll(String language);
}
