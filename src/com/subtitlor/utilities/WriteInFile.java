package com.subtitlor.utilities;

import com.subtitlor.beans.Subtitle;

import javax.servlet.ServletContext;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class WriteInFile {

    public static void write(List<Subtitle> content, String file) throws FileNotFoundException {

        BufferedWriter bw = null;
        FileWriter fw = null;
        int i = 0;

        try {

            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);

            while (i < content.size()){
                bw.write(content.get(i).getNum() + "\n");
                bw.write(content.get(i).getBeginEndSub() +"\n");
                bw.write(content.get(i).getFirstTextSub() +"\n");
                if (content.get(i).getScdTextSub() != null && !content.get(i).getScdTextSub().equals("")){
                    bw.write(content.get(i).getScdTextSub() +"\n");
                }
                bw.write(" \n");
                i += 1;
            }

        } catch (IOException e) {
            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
