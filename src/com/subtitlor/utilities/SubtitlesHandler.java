package com.subtitlor.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SubtitlesHandler {
    private ArrayList<String> originalSubtitles = null;

    public SubtitlesHandler(String fileName) {
        originalSubtitles = new ArrayList<String>();
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.matches(".*[a-z].*") || line.matches(".*[A-Z].*")){
                    originalSubtitles.add(line);
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getSubtitles() {
        return originalSubtitles;
    }

}
