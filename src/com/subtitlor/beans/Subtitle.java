package com.subtitlor.beans;

public class Subtitle {
    private int num;
    private String beginEndSub;
    private String firstTextSub;
    private String scdTextSub;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getBeginEndSub() {
        return beginEndSub;
    }

    public void setBeginEndSub(String beginEndSub) {
        this.beginEndSub = beginEndSub;
    }

    public String getFirstTextSub() {
        return firstTextSub;
    }

    public void setFirstTextSub(String firstTextSub) {
        this.firstTextSub = firstTextSub;
    }

    public String getScdTextSub() {
        return scdTextSub;
    }

    public void setScdTextSub(String scdTextSub) {
        this.scdTextSub = scdTextSub;
    }
}
