1
00:00:00,535 --> 00:00:02,462
Je suis professeur d'informatique et
d'ingéniérie informatique

2
00:00:02,462 --> 00:00:04,389
à Carnegie Mellon,

3
00:00:04,389 --> 00:00:06,318
et mes recherches se concentrent sur
l'utilisabilité de la vie privée

4
00:00:06,318 --> 00:00:08,656
et de la sécurité,

5
00:00:08,656 --> 00:00:10,996
et mes amis aiment me donner des exemples

6
00:00:10,996 --> 00:00:13,198
de leurs frustrations liées
aux systèmes informatiques,

7
00:00:13,198 --> 00:00:16,552
particulièrement des frustrations liées à

8
00:00:16,552 --> 00:00:20,664
une vie privée et une sécurité
qui ne sont pas utilisables.

9
00:00:20,664 --> 00:00:23,375
J'entends donc beaucoup parler
des mots de passe.

10
00:00:23,375 --> 00:00:26,255
Beaucoup de gens sont irrités
par les mots de passe